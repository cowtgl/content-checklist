# Content Checklist for CERN Websites

A checklist for all those uploading content to CERN websites: [cern.ch/content-checklist](https://cern.ch/content-checklist)

---

This document is a work in progress. Please feel free to suggest changes by filing an issue or making a merge request.
