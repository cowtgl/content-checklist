---
lang: en-GB
title: Content Checklist for CERN Websites
---

## Meta data

1. [ ] Listing title: 60 characters
<details><summary>Details</summary>
    - Listing title does _not_ need to be the same as main title
</details>

1. [ ] Listing strap: 150 characters
<details><summary>Details</summary>
    - Listing strap does _not_ need to be the same as main strap -- it can be a teaser, since it is displayed on listings, while the main strap appears in context at the top of the article
</details>

1. [ ] Captions: 255 characters
<details><summary>Details</summary>
    - Please _do not_ exceed this limit. If you need to describe an image in great detail, include the description in the body of the text with a reference to the image in question
    - Also bear in mind that French texts tend to be longer, so try and avoid English captions that are more than 200 characters in length
</details>

1. [ ] Straps / captions: follow punctuation rules
<details><summary>Details</summary>
    - e.g., English straps / captions do not end with a full stop
</details>

1. [ ] Cross-check audience(s), topic and tag(s) <button type="button" class="btn btn-blue">home.cern</button>
<details><summary>Details</summary>
    - If in doubt, [contact the web editor](mailto:web-editor@cern.ch)
</details>

1. [ ] Tag official news <button type="button" class="btn btn-blue">home.cern</button>
<details><summary>Details</summary>
    - Use `Human resources` or `CHIS` wherever appropriate
</details>

1. [ ] Provide revision information <button type="button" class="btn btn-blue">home.cern</button>
<details><summary>Details</summary>
    - Please use one of these (required):
`[Fix typo]` • `[Correct error]` • `[Edit meta data]` • `[Update outdated content]` • `[Add new content]`
</details>

## Text content

1. [ ]  Check style and grammar
<details><summary>Details</summary>
    - No American spellings unless they are part of formal names. Check the [Oxford English Dictionary](https://oed.com/) if in doubt, but&hellip;
    - British English spellings take `-ise` variants instead of `-ize` ones, except when referring to CERN as "the Organization"
    - For *Italics*, EN text should use them *sparingly*, for emphasis. They can be used for *journal names* but not for quotes, where "quotation marks" do the job. In FR text, quotes need to be *italic* because quotes don't "close" before you see who said the quote, and then "reopen" afterwards. The lack of italics makes it harder to parse the info. 
</details>

1. [ ]  Check that all links are valid
<details><summary>Details</summary>
    - For all links to content on `home.cern`, remove `http(s)://home.cern` from the URL
</details>

1. [ ] Remove extraneous/incorrect formatting tags in the HTML code
<details><summary>Details</summary>
    - Switch to `Basic HTML` first, paste in the text (from LibreOffice or Word, e.g.), and then switch back to `CERN Full HTML`
    - Make sure there are no additional spaces, characters or tags (e.g. `<span>`) inserted in the text
</details>

1. [ ] Use `&nbsp;` instead of regular spaces where needed <button type="button" class="btn btn-green">a11y</button>
<details><summary>Details</summary>
    - Numbers with units (e.g. 500 kg) or dates (e.g. 22 November) should have `&nbsp;` instead of regular spaces. e.g.: `500&nbsp;kg` or `22&nbsp;November`
    - For _French_ text, quotation marks and colons should have `&nbsp;` as well: `«&nbsp;`, `&nbsp;»` and `&nbsp;:`
</details>

1. [ ] Replace dashes for showing range with the words "to" or "and" <button type="button" class="btn btn-green">a11y</button>
<details><summary>Details</summary>
    - Say "6 and 7 June" instead of "6&ndash;7 June" or "6 to 9 June" instead of "6&ndash;9 June" for those with screen readers
</details>

1. [ ] Use correct symbols: minus / dash / hyphen and degrees <button type="button" class="btn btn-green">a11y</button>
<details><summary>Details</summary>
    - A minus (&minus;), an en&nbsp;dash (&ndash;) and a hyphen (\-) are rendered differently and are read differently by screen readers. Use the appropriate symbol
        - Particularly important when the minus (`&minus;`) is supposed to be in a superscript: 10<sup>&minus;32</sup>, not 10<sup>-32</sup>
        - Separators, when used, should be rendered as en-dashes (&ndash;) using `&ndash;`, not hyphens
        - Do _not_ use em&nbsp;dashes (&mdash;).
    - Do not use a superscripted `0` (<sup>0</sup>C) or a superscripted `o` (<sup>o</sup>C) when you mean to use the degree symbol (&deg;C): `&deg;`
</details>

1. [ ] Use pretty punctuation
<details><summary>Details</summary>
    - Don't use typewriter quotes, but "pretty" quotes instead: \"text\" vs &ldquo;text&rdquo;
    - The same goes for apostrophes: l\'air vs l&rsquo;air
</details>

1. [ ] Use [MathJax](https://www.mathjax.org/) for LaTeX equations, but only if required
<details><summary>Details</summary>
    - MathJax allows you to display equations and scientific notations in HTML
    - **Usage** is very simple. You bookend your LaTex code with dollar signs: `$YOUR-LATEX-CODE$`. For example, `$B_s^0$` will convert to “B-subscript-s-superscript-0”. If you are confused about the exact LaTex code to use, please consult your friendly neighbourhood ~~Spiderman~~ physicist
    - Please use it when required (and only when required); use [unicode](https://home.unicode.org/) as a first option always (see [list of unicode characters](https://en.wikipedia.org/wiki/List_of_Unicode_characters)).
    - However, **do not be tempted** to replace regular unicode/HTML characters with Latex where unnecessary: `$\mu$` will convert to the Greek letter µ with MathJax, but this is already a valid unicode character and should be pasted in as is (or using the HTML code `&mu;` when entering text in the HTML editor in Drupal). See, for example, [Achintya’s article on the LHCb tetraquark](https://home.cern/news/news/physics/lhcb-discovers-first-open-charm-tetraquark/), which uses pure unicode (including arrows and non-breaking spaces).
</details>

## Multimedia content

1. [ ] Check if the image is on CDS first before uploading a local image or a special-category CDS image
<details><summary>Details</summary>
    - Ask those who submit images as part of the article if they are already on CDS
</details>

1. [ ] Optimise images before uploading (local or CDS) <button type="button" class="btn btn-green">a11y</button>
<details><summary>Details</summary>
    - **CDS images**: _Minimum_ width of 1440 px at 72 pixels/inch
    - **Local images**: _Maximum_ width of 1440 px at 72 pixels/inch
    - Reduce the quality by optimising the image using (1) [ImageOptim](https://imageoptim.com/online) or (2) [Squoosh](https://squoosh.app/)
    - When uploading to CDS, provide clear descriptions, and assign credit correctly
    - When uploading a local image, _please provide correct alt text_ <button type="button" class="btn btn-green">a11y</button>
</details>

1. [ ] Link credits in captions (for inline images and videos) to CDS / `videos.cern.ch`
<details><summary>Details</summary>
    - You may need to modify the `source` of the article to add the link
</details>

1. [ ] Embed CDS slideshow at end of article
<details><summary>Details</summary>
    - Go to the CDS entry, click on `View as Slideshow` and then click on `Embed record as a slideshow` below the images
    - Cross-link back to the CDS entry
</details>

1. [ ] Check image sizes for listing and main images
<details><summary>Details</summary>
    - Listing images from CDS should be `medium` (and in some cases `small`) but never `large`. It depends on how pixelated the image looks. Some look fine with small, others need medium.
    - Main images should _always_ be `large`
</details>

1. [ ]  YouTube videos should include `?rel=0` at the end of the URL
<details><summary>Details</summary>
    - Also choose the "privacy friendly" embed code when possible
    - If subtitles are available, force them to display by appending `&amp;cc_lang_pref=en&amp;cc_load_policy=1` to `?rel=0`, where `cc_lang_pref` should be `en` or `fr` depending on the article's language
</details>

1. [ ]  Videos to no longer mention the editor
<details><summary>Details</summary>
    - Should only say `Video: CERN` (in EN) and `Video : CERN` (in FR).
</details>

1. [ ] [**After publishing**] Contact APS to ask them to cross-link to article
<details><summary>Details</summary>
    - This improves content discovery, when combined with slideshow embeds and linking credits
</details>

---

_More coming soon!_

---

## Notes, references and further reading

1. IR-ECO-CO Writing Guidelines: [cern.ch/writing-guidelines](https://cern.ch/writing-guidelines)
1. CERN Style Guide for official documents:
    1. English: [PDF](http://translation-council-support-group.web.cern.ch/sites/translation-council-support-group.web.cern.ch/files/styles/CERN%20TM%20English%20language%20style%20guide.pdf)
    1. French: [PDF](http://translation-council-support-group.web.cern.ch/sites/translation-council-support-group.web.cern.ch/files/styles/CERN%20TM%20English%20language%20style%20guide.pdf)
1. Accessibility / a11y:
    1. [The A11Y Project](https://a11yproject.com)
    1. [Mozilla web docs on Accessibility](https://developer.mozilla.org/en-US/docs/Learn/Accessibility)
    1. [Mozilla web docs on Accessibility in HTML](https://developer.mozilla.org/en-US/docs/Learn/Accessibility/HTML)
    1. [Writing effective alt text](https://www2.le.ac.uk/webcentre/plone/build/basics/add-images/alt-text)
1. [W3C Character Entity Reference Chart](https://dev.w3.org/html5/html-author/charref)
1. About checklists:
    1. [The Checklist](https://www.newyorker.com/magazine/2007/12/10/the-checklist)
    1. [A Lifesaving Checklist](https://www.nytimes.com/2007/12/30/opinion/30gawande.html)

